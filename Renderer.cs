﻿using System.Drawing;

using System.Windows.Forms;
using System.Numerics;
using Vertex = System.Numerics.Vector2;


namespace MatrixFilters
{
    class Renderer
    {
        private DirectBitmap[] Bitmaps;
        private DirectBitmap CurrentBitmap { get { return Bitmaps[CurrentBitmapIndex]; } }
        private PictureBox Screen { get; }
        public int Height { get; }
        public int Width { get; }

        private int CurrentBitmapIndex;

        public Renderer(int width, int height, PictureBox screen)
        {
            Screen = screen;
            Width = width;
            Height = height;
            Bitmaps = new DirectBitmap[2];
            Bitmaps[0] = new DirectBitmap(width, height);
            Bitmaps[1] = new DirectBitmap(width, height);
            CurrentBitmapIndex = 0;
        }

        public Renderer(PictureBox screen)
        {
            Screen = screen;
            Width = screen.Width;
            Height = screen.Height;
            Bitmaps = new DirectBitmap[2];
            Bitmaps[0] = new DirectBitmap(Width, Height);
            Bitmaps[1] = new DirectBitmap(Width, Height);
            CurrentBitmapIndex = 0;
        }

        public void Clear(byte value)
        {
            CurrentBitmap.Clear(value);
        }

        public void Draw(IDrawable drawable)
        {
            drawable.Draw(this);
        }

        public void DrawVertex(Vertex vertex)
        {
            CurrentBitmap.Graphics.DrawEllipse(Pens.Black, vertex.X - 2, vertex.Y - 2, 4, 4);
        }

        public void DrawLine(Vertex a, Vertex b)
        {
            CurrentBitmap.Graphics.DrawLine(Pens.Black, a.X, a.Y, b.X, b.Y);
        }

        public Vector3 VertexToNormalizedCoords(int x, int y)
        {
            var outX = (x * 2 / (float)Width) - 1;
            var outY = (y * 2 / (float)Height) - 1;

            Vector3 pos = (new Vector3(outX, outY, 0));
            return pos;
        }

        public void Display()
        {
            Screen.Image = CurrentBitmap.Bitmap;
            CurrentBitmapIndex ^= 1;
        }

        public void SetPixel(int x, int y, Color color) => CurrentBitmap.SetPixel(x, y, color);
        public void SetAlpha(int x, int y, int val) => CurrentBitmap.SetAlpha(x, y, val);
    }


}
