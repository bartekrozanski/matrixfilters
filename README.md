# What is it?
Simple winforms app for editing photos using matrix filters

<img src="screenshots/screenshot1.PNG" width="500" height="300" />
<img src="screenshots/screenshot2.PNG" width="500" height="300" />
<img src="screenshots/screenshot3.PNG" width="500" height="300" />

# How to build?
Clone this repo, launch .sln file with Visual Studio and you are good to go!
