﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MatrixFilters
{
    public partial class Form1 : Form
    {
        MatrixFilter _filter;
        Selection selection;
        DirectPhoto ActivePhoto;
        DirectBitmap workingBitmap;
        Renderer Renderer;
        bool WholePhoto = true;
        int brushSize;

        MatrixFilter Filter
        {
            get { return _filter; }
            set
            {
                _filter = value;
                _filter.UpdateDivisor();
                synchonizeFilterWithUi();
            }
        }

        public Form1()
        {
            InitializeComponent();
            InitializeNumericsUpDowns();
            Renderer = new Renderer(window);
            radioButton4.Checked = true;
            radioButton4.PerformClick();
            trackBar1_Scroll(trackBar1, null);
            LockAllTools();
        }

        private void LockAllTools()
        {
            foreach (Control control in splitContainer1.Panel2.Controls)
            {
                control.Enabled = false;
            }
            foreach (Control control in splitContainer2.Panel1.Controls)
            {
                control.Hide();
            }
        }
        private void UnlockAllTools()
        {
            foreach (Control control in splitContainer1.Panel2.Controls)
            {
                control.Enabled = true;
            }
            foreach (Control control in splitContainer2.Panel1.Controls)
            {
                control.Show();
            }
        }
        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var selectFileDialog = new OpenFileDialog())
            {
                if (selectFileDialog.ShowDialog() == DialogResult.OK)
                {
                    var loadedImage = Image.FromFile(selectFileDialog.FileName);
                    var size = new Size(loadedImage.Width, loadedImage.Height);
                    if(size.Width > window.Width || size.Height > window.Height)
                    {
                        double widthRatio = size.Width / (double)window.Width;
                        double heightRatio = size.Height / (double)window.Height;

                        var k = Math.Max(heightRatio, widthRatio);
                        size.Width = (int)(size.Width / k);
                        size.Height = (int)(size.Height/ k);

                    }
                    ActivePhoto = new DirectPhoto(new Bitmap(loadedImage, size),0, 0);
                    workingBitmap = new DirectBitmap(ActivePhoto.DirectBitmap);
                    selection = new Selection(workingBitmap);
                    selection.SelectAll();
                    UnlockAllTools();
                    UpdateHistograms();
                    Draw();
                }
            }
        }

        private void synchonizeFilterWithUi()
        {
            numericUpDown1.Value = Filter[0, 0];
            numericUpDown2.Value = Filter[0, 1];
            numericUpDown3.Value = Filter[0, 2];
            numericUpDown4.Value = Filter[1, 0];
            numericUpDown5.Value = Filter[1, 1];
            numericUpDown6.Value = Filter[1, 2];
            numericUpDown7.Value = Filter[2, 0];
            numericUpDown8.Value = Filter[2, 1];
            numericUpDown9.Value = Filter[2, 2];
            label1.Text = _filter.divisor.ToString();
        }
        private void UpdateHistograms()
        {
            chart1.Series.Clear();
            chart2.Series.Clear();
            chart3.Series.Clear();
            int[][] bins = new int[3][];
            bins[0] = new int[256];
            bins[1] = new int[256];
            bins[2] = new int[256];
            var redSeries = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "red",
                Color = Color.Red
            };
            var greenSeries = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "green",
                Color = Color.Green
            };
            greenSeries.Color = Color.Green;
            var blueSeries = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "blue",
                Color = Color.Blue
            };

            for (int y = 0; y < ActivePhoto.Height; ++y)
            {
                for(int x = 0; x < ActivePhoto.Width; ++x)
                {
                    bins[0][ActivePhoto.GetPixel(x, y).R]++;
                    bins[1][ActivePhoto.GetPixel(x, y).G]++;
                    bins[2][ActivePhoto.GetPixel(x, y).B]++;
                }
            }
            int maxVal = Math.Max(Math.Max(bins[0].Max(), bins[1].Max()), bins[2].Max());

            for (int bin = 0; bin < 256; ++bin)
            {
                redSeries.Points.Add(new System.Windows.Forms.DataVisualization.Charting.DataPoint(bin, bins[0][bin]));
                greenSeries.Points.Add(new System.Windows.Forms.DataVisualization.Charting.DataPoint(bin, bins[1][bin]));
                blueSeries.Points.Add(new System.Windows.Forms.DataVisualization.Charting.DataPoint(bin, bins[2][bin]));
            }
            chart1.ChartAreas[0].AxisY.Maximum = maxVal;
            chart1.ChartAreas[0].AxisX.Minimum = 0;
            chart1.ChartAreas[0].AxisX.Maximum= 255;
            chart2.ChartAreas[0].AxisY.Maximum = maxVal;
            chart2.ChartAreas[0].AxisX.Minimum = 0;
            chart2.ChartAreas[0].AxisX.Maximum = 255;
            chart3.ChartAreas[0].AxisY.Maximum = maxVal;
            chart3.ChartAreas[0].AxisX.Minimum = 0;
            chart3.ChartAreas[0].AxisX.Maximum = 255;
            chart1.Series.Add(redSeries);
            chart2.Series.Add(greenSeries);
            chart3.Series.Add(blueSeries);
        }

        private void ApplyFilter()
        {
            workingBitmap.CopyRawData(ActivePhoto.DirectBitmap);
            for(int x = 0; x < ActivePhoto.Width; ++x)
            {
                for (int y = 0; y < ActivePhoto.Height; ++y)
                {
                    if (selection.IsSelected(x,y))
                    {
                        int r, g, b;
                        r = g = b = 0;
                        for(int k = -1; k < 2; ++k)
                        {
                            for(int m = -1; m < 2; ++m)
                            {
                                var currentColor = workingBitmap.GetPixel(x + m, y + k);
                                r += currentColor.R * Filter[k + 1, m + 1];
                                g += currentColor.G * Filter[k + 1, m + 1];
                                b += currentColor.B * Filter[k + 1, m + 1];
                            }
                        }
                        if(Filter.divisor != 0)
                        {
                            r = (int)(r * Filter.divisor);
                            g = (int)(g * Filter.divisor);
                            b = (int)(b * Filter.divisor);
                        }
                        r = Clamp(r, 0, 255);
                        g = Clamp(g, 0, 255);
                        b = Clamp(b, 0, 255);

                        Color outColor = Color.FromArgb(r, g, b);
                        ActivePhoto.SetPixel(x, y, outColor);
                    }
                }
            }
        }

        private void Draw()
        {
            Renderer.Clear(240);
            Renderer.Draw(ActivePhoto);
            if(!WholePhoto) Renderer.Draw(selection);
            Renderer.Display();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            var castedSeneder = (RadioButton)sender;
            if (castedSeneder.Checked)
            {
                Filter = MatrixFilter.IdentityFilter();
                castedSeneder.PerformClick();
            }
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            var castedSeneder = (RadioButton)sender;
            if (castedSeneder.Checked)
            {
                Filter = MatrixFilter.BlurFilter();
                castedSeneder.PerformClick();
            }
        }

        public int Clamp(int val, int min, int max)
        {
            //to feel like a hackerman :C
            return val <= max && val >= min ? val : (val > max ? max : min); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ApplyFilter();
            UpdateHistograms();
            if(!WholePhoto)
            {
                selection.UnselectAll();
            }
            Draw();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            numericChange(0, 0, ((NumericUpDown)sender).Value);

        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            var castedSender = (NumericUpDown)sender;
            numericChange(0, 1, ((NumericUpDown)sender).Value);

        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            numericChange(0, 2, ((NumericUpDown)sender).Value);

        }
        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            numericChange(1, 0, ((NumericUpDown)sender).Value);

        }
        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            numericChange(1, 1, ((NumericUpDown)sender).Value);

        }
        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            numericChange(1, 2, ((NumericUpDown)sender).Value);

        }
        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            numericChange(2, 0, ((NumericUpDown)sender).Value);

        }
        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            numericChange(2, 1, ((NumericUpDown)sender).Value);

        }
        private void numericUpDown9_ValueChanged(object sender, EventArgs e)
        {
            numericChange(2, 2, ((NumericUpDown)sender).Value);
        }

        private void numericChange(int row, int col, decimal val)
        {
            _filter[row, col] = (int)val;
            synchonizeFilterWithUi();
            if(!radioButton9.Checked)
            {
                radioButton9.PerformClick();
            }
        }
        private void InitializeNumericsUpDowns()
        {
            List<NumericUpDown> list = new List<NumericUpDown>();
            list.Add(numericUpDown1);
            list.Add(numericUpDown2);
            list.Add(numericUpDown3);
            list.Add(numericUpDown4);
            list.Add(numericUpDown5);
            list.Add(numericUpDown6);
            list.Add(numericUpDown7);
            list.Add(numericUpDown8);
            list.Add(numericUpDown9);
            foreach (var control in list)
            {
                control.Maximum = 100M;
                control.Minimum = -100M;
                control.Increment = 1M;
            }
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            var castedSeneder = (RadioButton)sender;
            if (castedSeneder.Checked)
            {
                Filter = MatrixFilter.EdgeDetectionFilter();
                castedSeneder.PerformClick();
            }
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            var castedSeneder = (RadioButton)sender;
            if (castedSeneder.Checked)
            {
                Filter = MatrixFilter.CarvingFilter();
                castedSeneder.PerformClick();
            }
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            var castedSeneder = (RadioButton)sender;
            if (castedSeneder.Checked)
            {
                Filter = MatrixFilter.SharpeningFilter();
                castedSeneder.PerformClick();
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            WholePhoto = false;
            selection.UnselectAll();
            Draw();
        }


        private void window_MouseMove(object sender, MouseEventArgs e)
        {
            selection.SelectPointsInCircle(e.X, e.Y, brushSize);
            Draw();
        }

        private void window_MouseDown(object sender, MouseEventArgs e)
        {
            if (!WholePhoto)
            {
                window.MouseMove += window_MouseMove;
                window.MouseUp += window_MouseUp;
            }
        }

        private void window_MouseUp(object sender, MouseEventArgs e)
        {
            window.MouseMove -= window_MouseMove;
            window.MouseUp -= window_MouseUp;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            brushSize = ((TrackBar)sender).Value;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            WholePhoto = true;
            selection.SelectAll();
        }
    }
}
