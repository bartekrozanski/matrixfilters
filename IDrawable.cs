﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixFilters
{
    interface IDrawable
    {
        void Draw(Renderer renderTarget);
    }
}
