﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MatrixFilters
{
    class Selection : IDrawable
    {
        DirectBitmap selection;
        public Selection(DirectBitmap bitmap)
        {
            selection = new DirectBitmap(bitmap.Width, bitmap.Height);
        }
        public bool IsSelected(int x, int y)
        {
            return selection.GetPixel(x, y).R == 0;
        }
        public void Clear(byte val) => selection.Clear(val);
        public void SelectAll() => Clear(0);
        public void UnselectAll() => Clear(255);
        public void SelectPointsInCircle(int x, int y, int radius)
        {
            selection.Graphics.FillEllipse(Brushes.Black, x - radius, y - radius, 2 * radius, 2 * radius);
        }
        public void Draw(Renderer renderTarget)
        {
            for(int x = 0; x < selection.Width; ++x)
            {
                for (int y = 0; y < selection.Height; ++y)
                    if(IsSelected(x,y))
                    {
                        renderTarget.SetAlpha(x, y, 200);
                    }
            }
        }
    }
}
