﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixFilters
{
    class MatrixFilter
    {
        int[] vals;
        public double divisor { get; private set; }

        public void UpdateDivisor()
        {
            var sum = vals.Sum();
            if(sum == 0)
            {
                divisor = 0;
            }
            else
            {
                divisor = 1.0 / vals.Sum();
            }
        }

        public int this[int row, int col]
        {
            get { return vals[row * 3 + col]; }
            set {
                vals[row * 3 + col] = value;
                UpdateDivisor();
            }
        }
        private MatrixFilter()
        {
            vals = new int[9];
            Array.Clear(vals, 0, 9);
        }

        public static MatrixFilter IdentityFilter()
        {
            var matrix = new MatrixFilter();
            matrix[1, 1] = 1;
            matrix.UpdateDivisor();

            return matrix;
        }

        public static MatrixFilter BlurFilter()
        {
            var matrix = new MatrixFilter();
            for (int i = 0; i < 3; ++i)
            {
                matrix[1, i] = 1;
                matrix[i, 1] = 1;
            }
            matrix.UpdateDivisor();
            return matrix;
        }

        public static MatrixFilter EdgeDetectionFilter()
        {
            var matrix = new MatrixFilter();
            for (int i = 0; i < 3; ++i)
            {
                matrix[0, i] = -1;
                matrix[i, 0] = -1;
                matrix[i, 2] = -1;
                matrix[2, i] = -1;
            }
            matrix[1, 1] = 8;
            matrix.UpdateDivisor();
            return matrix;
        }

        public static MatrixFilter SharpeningFilter()
        {
            var matrix = new MatrixFilter();
            for (int i = 0; i < 3; ++i)
            {
                matrix[1, i] = -1;
                matrix[i, 1] = -1;
            }
            matrix[1, 1] = 5;
            matrix.UpdateDivisor();
            return matrix;
        }
        public static MatrixFilter CarvingFilter()
        {
            var matrix = new MatrixFilter();
            matrix[2, 2] = 1;
            matrix[1, 2] = 1;
            matrix[2, 1] = 1;
            matrix[1, 1] = 1;
            matrix[0, 0] = -1;
            matrix[1, 0] = -1;
            matrix[0, 1] = -1;
            matrix.UpdateDivisor();
            return matrix;
        }
    }
}
