﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace MatrixFilters
{
    class DirectPhoto : IDrawable
    {
        public DirectBitmap DirectBitmap { get; private set; }
        int PosX { get; set; }
        int PosY { get; set; }
        public int Width { get { return DirectBitmap.Width; } }
        public int Height { get { return DirectBitmap.Height; } }
        public Color GetPixel(int x, int y) => DirectBitmap.GetPixel(x, y);
        public void SetPixel(int x, int y, Color color) => DirectBitmap.SetPixel(x, y, color);


        public DirectPhoto(Bitmap source, int posX, int posY)
        {
            DirectBitmap = new DirectBitmap(source);
            PosX = posX;
            PosY = posY;
        }

        public DirectPhoto(int width, int height, int posX, int posY)
        {
            DirectBitmap = new DirectBitmap(width, height);
            DirectBitmap.Clear(255);
            PosX = posX;
            PosY = posY;
        }

        public void Draw(Renderer renderTarget)
        {
            for (int y = 0 + PosY; y < Height + PosY; ++y)
            {
                for(int x = 0 + PosX; x <PosX + Width; ++x)
                {
                    renderTarget.SetPixel(x, y, GetPixel(x, y));
                }
            }
        }
    }
}
